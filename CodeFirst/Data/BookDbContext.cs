using System;
using Microsoft.EntityFrameworkCore;
using CodeFirst.Models;
namespace CodeFirst.Data
{
    public class BookDbContext : DbContext
    {

        public BookDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Book> Book { get; set; }
        public DbSet<Author> Author { get; set; }
       protected override void OnModelCreating(ModelBuilder modelBuilder){
           base.OnModelCreating(modelBuilder);
            var books= new Book[]{
                new Book(){BookId = 1 ,  Title =" Book 1 " ,Avaialbe = true ,Price  = 20.00 ,PublishedAt = DateTime.Now , CoverImg = "/images/1.jpg"},
                new Book(){BookId = 2 ,  Title =" Book 2 " ,Avaialbe = false ,Price  = 50.00 ,PublishedAt = DateTime.Now.AddDays(5) , CoverImg = "images/2.jpg"}
            };
            var Authors = new Author[]{
                new Author(){ID = 1 , Name = "Ahmed" , Age = 23 ,Address ="Address  1 ", Avialable = DateTime.Now.AddDays(2)},
                new Author(){ID = 2 , Name = "Omar" , Age = 22 ,Address ="Address  2 ", Avialable = DateTime.Now.AddDays(5)}
            };
            modelBuilder.Entity<Book>().HasData(books);
            modelBuilder.Entity<Author>().HasData(Authors);
       }

    }
}