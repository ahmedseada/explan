using System;
using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Author
    {
        [Key]
        [Required]
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public DateTime Avialable { get; set; }
        
    }
}