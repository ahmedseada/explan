using System ;
namespace CodeFirst.Models
{
    public class Book{
        public int BookId { get; set; }
        public string Title { get; set; }
        public Author AuthorId { get; set; }
        public DateTime PublishedAt { get; set; }
        public double Price { get; set; }
        public bool Avaialbe { get; set; }
        public string CoverImg { get; set; }
    }
}