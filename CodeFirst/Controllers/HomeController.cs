﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CodeFirst.Data;
using CodeFirst.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace CodeFirst.Controllers
{
    public class HomeController : Controller
    {
        private readonly BookDbContext context;
        private readonly IWebHostEnvironment env;

        public HomeController(BookDbContext context, IWebHostEnvironment env)
        {
            this.context = context;
            this.env = env;
        }

        public IActionResult Index()
        {
            var books = context.Book.ToList();
            ViewBag.saved = TempData["saved"];
            return View(books);
        }
        public IActionResult Details(int id)
        {
            ViewBag.saved = TempData["saved"];
            return View(context.Book.FirstOrDefault(b => b.BookId == id));
        }
        [HttpPost]
        public IActionResult Details(IFormFile img, int id)
        {
            if (img != null)
            {
                var fname = System.Guid.NewGuid().ToString().Substring(0, 8) + Path.GetExtension(img.FileName);  //asasa.jpg
                var fpath = Path.Combine(env.WebRootPath, "images", fname);
                img.CopyTo(new FileStream(fpath, FileMode.Create));
                var book = context.Book.FirstOrDefault(b => b.BookId == id);
                book.CoverImg = "/images/"+fname;
                context.Book.Update(book);
                context.SaveChanges();
            }
            TempData["saved"] = "Your Changes Saved";
            return RedirectToAction("Details",new {id = id});
        }
        public IActionResult Privacy()
        {


            return View();
        }
        
        public IActionResult ShowBook(){
            var book  = context.Book.FirstOrDefault(b => b.BookId == 1);
            return View(book);
        }
    [HttpPost]
    public IActionResult ShowBook(int id , IFormFile img){
        if (img !=null)
        {
            var fname = System.Guid.NewGuid().ToString() + Path.GetExtension(img.FileName);//.jpm png
            var fpath = Path.Combine(env.WebRootPath,"images",fname);
            img.CopyTo(new FileStream(fpath,FileMode.Create));
            var book  = context.Book.FirstOrDefault(b => b.BookId == 1);
            book.CoverImg = "/images/"+fname;
            context.Book.Update(book);
            context.SaveChanges();
            return RedirectToAction("showbook");
        }

            return View();
    }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
