﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeFirst.Migrations
{
    public partial class Second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "ID", "Age", "Name" },
                values: new object[] { 125, 23, "Ahmed" });

            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "ID", "Age", "Name" },
                values: new object[] { 126, 22, "Omar" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 125);

            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 126);
        }
    }
}
