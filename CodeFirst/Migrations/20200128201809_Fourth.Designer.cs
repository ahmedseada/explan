﻿// <auto-generated />
using System;
using CodeFirst.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CodeFirst.Migrations
{
    [DbContext(typeof(BookDbContext))]
    [Migration("20200128201809_Fourth")]
    partial class Fourth
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CodeFirst.Models.Author", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Age")
                        .HasColumnType("int");

                    b.Property<DateTime>("Avialable")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID");

                    b.ToTable("Author");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            Address = "Address  1 ",
                            Age = 23,
                            Avialable = new DateTime(2020, 1, 30, 22, 18, 8, 715, DateTimeKind.Local).AddTicks(2020),
                            Name = "Ahmed"
                        },
                        new
                        {
                            ID = 2,
                            Address = "Address  2 ",
                            Age = 22,
                            Avialable = new DateTime(2020, 2, 2, 22, 18, 8, 715, DateTimeKind.Local).AddTicks(3001),
                            Name = "Omar"
                        });
                });

            modelBuilder.Entity("CodeFirst.Models.Book", b =>
                {
                    b.Property<int>("BookId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AuthorIdID")
                        .HasColumnType("int");

                    b.Property<bool>("Avaialbe")
                        .HasColumnType("bit");

                    b.Property<double>("Price")
                        .HasColumnType("float");

                    b.Property<DateTime>("PublishedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Title")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("BookId");

                    b.HasIndex("AuthorIdID");

                    b.ToTable("Book");

                    b.HasData(
                        new
                        {
                            BookId = 1,
                            Avaialbe = true,
                            Price = 20.0,
                            PublishedAt = new DateTime(2020, 1, 28, 22, 18, 8, 712, DateTimeKind.Local).AddTicks(7916),
                            Title = " Book 1 "
                        },
                        new
                        {
                            BookId = 2,
                            Avaialbe = false,
                            Price = 50.0,
                            PublishedAt = new DateTime(2020, 2, 2, 22, 18, 8, 714, DateTimeKind.Local).AddTicks(7151),
                            Title = " Book 2 "
                        });
                });

            modelBuilder.Entity("CodeFirst.Models.Book", b =>
                {
                    b.HasOne("CodeFirst.Models.Author", "AuthorId")
                        .WithMany()
                        .HasForeignKey("AuthorIdID");
                });
#pragma warning restore 612, 618
        }
    }
}
