﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeFirst.Migrations
{
    public partial class Sixth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 1,
                column: "Avialable",
                value: new DateTime(2020, 1, 30, 23, 56, 24, 387, DateTimeKind.Local).AddTicks(1440));

            migrationBuilder.UpdateData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 2,
                column: "Avialable",
                value: new DateTime(2020, 2, 2, 23, 56, 24, 387, DateTimeKind.Local).AddTicks(1968));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "BookId",
                keyValue: 1,
                column: "PublishedAt",
                value: new DateTime(2020, 1, 28, 23, 56, 24, 385, DateTimeKind.Local).AddTicks(5278));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "BookId",
                keyValue: 2,
                columns: new[] { "CoverImg", "PublishedAt" },
                values: new object[] { "images/2.jpg", new DateTime(2020, 2, 2, 23, 56, 24, 386, DateTimeKind.Local).AddTicks(9222) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 1,
                column: "Avialable",
                value: new DateTime(2020, 1, 30, 23, 27, 28, 852, DateTimeKind.Local).AddTicks(7668));

            migrationBuilder.UpdateData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 2,
                column: "Avialable",
                value: new DateTime(2020, 2, 2, 23, 27, 28, 852, DateTimeKind.Local).AddTicks(8176));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "BookId",
                keyValue: 1,
                column: "PublishedAt",
                value: new DateTime(2020, 1, 28, 23, 27, 28, 851, DateTimeKind.Local).AddTicks(2295));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "BookId",
                keyValue: 2,
                columns: new[] { "CoverImg", "PublishedAt" },
                values: new object[] { "/images/2.jpg", new DateTime(2020, 2, 2, 23, 27, 28, 852, DateTimeKind.Local).AddTicks(5079) });
        }
    }
}
