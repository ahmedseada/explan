﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeFirst.Migrations
{
    public partial class Third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 125);

            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 126);

            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "ID", "Age", "Name" },
                values: new object[] { 27, 23, "Abdo" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 27);

            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "ID", "Age", "Name" },
                values: new object[] { 125, 23, "Ahmed" });

            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "ID", "Age", "Name" },
                values: new object[] { 126, 22, "Omar" });
        }
    }
}
