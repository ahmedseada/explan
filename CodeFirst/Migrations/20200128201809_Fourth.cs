﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeFirst.Migrations
{
    public partial class Fourth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 27);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Author",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Avialable",
                table: "Author",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "ID", "Address", "Age", "Avialable", "Name" },
                values: new object[,]
                {
                    { 1, "Address  1 ", 23, new DateTime(2020, 1, 30, 22, 18, 8, 715, DateTimeKind.Local).AddTicks(2020), "Ahmed" },
                    { 2, "Address  2 ", 22, new DateTime(2020, 2, 2, 22, 18, 8, 715, DateTimeKind.Local).AddTicks(3001), "Omar" }
                });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "BookId", "AuthorIdID", "Avaialbe", "Price", "PublishedAt", "Title" },
                values: new object[,]
                {
                    { 1, null, true, 20.0, new DateTime(2020, 1, 28, 22, 18, 8, 712, DateTimeKind.Local).AddTicks(7916), " Book 1 " },
                    { 2, null, false, 50.0, new DateTime(2020, 2, 2, 22, 18, 8, 714, DateTimeKind.Local).AddTicks(7151), " Book 2 " }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookId",
                keyValue: 2);

            migrationBuilder.DropColumn(
                name: "Address",
                table: "Author");

            migrationBuilder.DropColumn(
                name: "Avialable",
                table: "Author");

            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "ID", "Age", "Name" },
                values: new object[] { 27, 23, "Abdo" });
        }
    }
}
