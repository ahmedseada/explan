﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeFirst.Migrations
{
    public partial class Fifth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CoverImg",
                table: "Book",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 1,
                column: "Avialable",
                value: new DateTime(2020, 1, 30, 23, 27, 28, 852, DateTimeKind.Local).AddTicks(7668));

            migrationBuilder.UpdateData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 2,
                column: "Avialable",
                value: new DateTime(2020, 2, 2, 23, 27, 28, 852, DateTimeKind.Local).AddTicks(8176));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "BookId",
                keyValue: 1,
                columns: new[] { "CoverImg", "PublishedAt" },
                values: new object[] { "/images/1.jpg", new DateTime(2020, 1, 28, 23, 27, 28, 851, DateTimeKind.Local).AddTicks(2295) });

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "BookId",
                keyValue: 2,
                columns: new[] { "CoverImg", "PublishedAt" },
                values: new object[] { "/images/2.jpg", new DateTime(2020, 2, 2, 23, 27, 28, 852, DateTimeKind.Local).AddTicks(5079) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CoverImg",
                table: "Book");

            migrationBuilder.UpdateData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 1,
                column: "Avialable",
                value: new DateTime(2020, 1, 30, 22, 18, 8, 715, DateTimeKind.Local).AddTicks(2020));

            migrationBuilder.UpdateData(
                table: "Author",
                keyColumn: "ID",
                keyValue: 2,
                column: "Avialable",
                value: new DateTime(2020, 2, 2, 22, 18, 8, 715, DateTimeKind.Local).AddTicks(3001));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "BookId",
                keyValue: 1,
                column: "PublishedAt",
                value: new DateTime(2020, 1, 28, 22, 18, 8, 712, DateTimeKind.Local).AddTicks(7916));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "BookId",
                keyValue: 2,
                column: "PublishedAt",
                value: new DateTime(2020, 2, 2, 22, 18, 8, 714, DateTimeKind.Local).AddTicks(7151));
        }
    }
}
